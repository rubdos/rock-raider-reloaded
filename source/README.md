Compiling and running
=====================

From git
--------

From git, the resources (textures and levels) needs 
to get precompiled.

You need Python 3 with pillow, freetype-py, pandas and numpy installed

    make

on any GNU system will work, when required dependencies are met.

Then,

    wmake

inside DOSBox will compile the rest.

From zipball
------------

    wmake

from within DOSBox will compile everything

Running
-------

Just type RR in the directory where make was called.
