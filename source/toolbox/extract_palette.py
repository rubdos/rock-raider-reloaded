from PIL import Image

def convert(c):
    res = c[0] * 2**16
    res = res + c[1] * 2**8
    res = res + c[2]
    return res

im = Image.open("color_palette.png")
pix = im.load()

a = []
for x in range(256):
    a.insert(x, convert(pix[x, 0]))
print(a)
