#!/usr/bin/env python3

import json
import struct
import sys

tilemap = {
        ' ': 0, # empty space
        'r': 1, # rubble
        'G': 2, # gravel
        'L': 3, # loose rock
        'H': 4, # hard rock
        'M': 5, # massive
        'C': 6, # crystal source
        'O': 7, # ore
        'w': 8, # water
        'l': 9, # lava
        's': 10, # slug
        'e': 11, # erosion
        'p': 12 # power path
        }

def uint8(inp):
    return struct.pack('B', inp)

in_filename = sys.argv[1]
data_filename = in_filename[:-5] + ".lvls"
out_filename = sys.argv[2]

output = open(out_filename, "wb")

level = json.load(open(in_filename, "r"))
data = open(data_filename, "r").readlines()
levelName = level['name']
width = level['width']
height = level['height']
goals = level['goals']

print("Compiling level \"" + levelName + "\" (" + str(width) + " x " + str(height) + ")")
print("Writing to", out_filename)

# Write header
output.write(b'RRlv')
output.write(uint8(width))
output.write(uint8(height))

# Write goals
_goals = ['energycrystals', 'building', 'vehicle']
for goal in _goals:
    if goal not in goals:
        goals[goal] = 0
    output.write(uint8(goals[goal]))
    del goals[goal]
if not 'position' in goals:
    goals['position'] = {"x": 0, "y": 0}
output.write(uint8(goals['position']['x']))
output.write(uint8(goals['position']['y']))
del goals['position']

if len(goals) > 0:
    print("Unrecognised goal. Must be one of " + str(_goals))

if not 'oxygenrate' in level:
    oxygenrate = 0
else:
    oxygenrate = level['oxygenrate']
output.write(uint8(oxygenrate))

if height != len(data):
    print("header height != data height")

linenumber = 0
for line in data:
    linenumber += 1
    line = line.strip()
    if len(line) != width:
        print("header width != line width (",linenumber,")")
    for c in line:
        output.write(uint8(tilemap[c]))
    print(line)
