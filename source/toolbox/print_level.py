#!/usr/bin/env python3
import sys
import struct

def uint8(b):
    return struct.unpack("B", b)[0]

f = open("../source/levels/" + sys.argv[1], "rb")
magic = f.read(4)
assert(magic == b'RRlv')
width = uint8(f.read(1))
height = uint8(f.read(1))

#goals
energy_crystals = uint8(f.read(1))
building_id = uint8(f.read(1))
vehicle_id = uint8(f.read(1))
x = uint8(f.read(1))
y = uint8(f.read(1))

print("Level: (" + str(width) + ",", str(height) + ")")
print("We have to get " + str(energy_crystals) + " energy crystal(s)")
if vehicle_id > 0:
    print("build a " + vehicles[vehicle_id])
if building_id > 0:
    print("build a " + buildings[building_id])
if x != 0 and y != 0:
    print("and go to (%d,%d)" % (x, y))

f.close()
