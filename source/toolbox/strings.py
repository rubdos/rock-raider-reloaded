#!/usr/bin/env python3

from misc import font
from collections import namedtuple
from misc import VGA
import struct

Text = namedtuple('Text', ['id', 'text', 'height', 'color', 'background'])

c = {
        'white': (255, 255, 255),
        'menu': (225, 225, 225),
        'black': (0, 0, 0,),
        'typebg': (16, 87, 7)
    }

# Add strings here:

strings = [
        Text('play',    "Play!",        50, c['menu'], c['black']),
        Text('penter',  "press enter",  25, c['menu'], c['black']),
        #Fieldtype strings
        Text('empty',   "Grond",        14, c['white'], c['typebg']),
        Text('rubble',  "Puin",         14, c['white'], c['typebg']),
        Text('gravel',  "Gruis",        14, c['white'], c['typebg']),
        Text('looserck',"Los Steen",    14, c['white'], c['typebg']),
        Text('hardrock', "Hard Steen",  14, c['white'], c['typebg']),
        Text('massive', "Massief Steen",14, c['white'], c['typebg']),
        Text('cryrock', "Kristalbron",  14, c['white'], c['typebg']),
        Text('orerock', "Ertsbron",     14, c['white'], c['typebg']),
        Text('water',   "Water",        14, c['white'], c['typebg']),
        Text('lava',    "Lava",         14, c['white'], c['typebg']),
        Text('snlhole', "Slakkenhol",   14, c['white'], c['typebg']),
        Text('coro',    "Erosie",       14, c['white'], c['typebg']),
        Text('power',   "Voedingsader", 14, c['white'], c['typebg'])
    ]



output = open("src/STRINGS.ASM", "w") # DOS likes capital chars.
include = open("src/STRINGS.INC", "w") # DOS likes capital chars.
include.write("GLOBAL loadAllStrings:PROC")

output.write(""";----------------------------------------------------------------------------;
; Python generated fonts file. Do not edit manually.
;----------------------------------------------------------------------------;
IDEAL
P386
MODEL FLAT, C
ASSUME cs:_TEXT,ds:FLAT,es:FLAT,fs:FLAT,gs:FLAT

INCLUDE "STRINGS.INC"
INCLUDE "SVGA.INC"
INCLUDE "LOADER.INC"

CODESEG

""")

dataseg = """
DATASEG
"""
udataseg = """
UDATASEG
"""

for string in strings:
    # Generate bitmap
    f = font.Font("toolbox/KabelITCbyBT.ttf", string.height)
    render = f.render_text(string.text)
    color = VGA.get_closest_index(string.color)
    background = VGA.get_closest_index(string.background)
    datasize = render.height * render.width

    # Write bitmap
    bmp = open("textures/text/%s.bmp" % string.id, "wb")
    bmp.write(struct.pack("I", render.width))
    bmp.write(struct.pack("I", render.height))
    for i in range(render.width * render.height):
        bmp.write(struct.pack("b", color if render.pixels[i] else background))

    # Writeout the datasegs
    udataseg += string.id + " dd 2 dup (?)\n"
    udataseg += (" " * len(string.id)) + " db " + str(datasize) + " dup (?) \n\n"
    dataseg += string.id + 'Meta db "textures\\text\\%s.bmp", 0 \n' % string.id

    # Writeout the draw functions
    procname = "draw%sText" % (string.id[0].upper() + string.id[1:])
    include.write(", %s:PROC" % procname)
    output.write("PROC %s\n" % procname)
    output.write("ARG @@x:dword,\\\n")
    output.write("    @@y:dword\n\n")
    output.write("    call drawTexture, offset " + string.id + ", [@@x], [@@y]\n")
    output.write("    ret\n")
    output.write("ENDP\n\n")

output.write("PROC loadAllStrings\n")
for string in strings:
    output.write("    call loadFile, offset %sMeta, offset %s \n" % (string.id, string.id))
output.write("    ret\n")
output.write("ENDP loadAllStrings \n\n")

output.write(dataseg)
output.write(udataseg)
output.write("END\n")
output.close()
include.close()
