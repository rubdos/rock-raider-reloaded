#!/usr/bin/env python3

from PIL import Image
import os
import misc.VGA
import struct

files = filter(lambda x : x.endswith(".png"), os.listdir("textures"))

output = open('src/TEX.ASM', 'w')
include = open('src/TEX.INC', 'w')
include.write("GLOBAL loadAllTextures:PROC")

output.write(""";----------------------------------------------------------------------------;
; Python generated texture file. Do not edit manually.
;----------------------------------------------------------------------------;
IDEAL
P386
MODEL FLAT, C
ASSUME cs:_TEXT,ds:FLAT,es:FLAT,fs:FLAT,gs:FLAT

INCLUDE "TEX.INC"
INCLUDE "SVGA.INC"
INCLUDE "LOADER.INC"

CODESEG

""")

dataseg = """
DATASEG
"""

udataseg = """
UDATASEG
"""

firstImage = True
texturesOnDisk = []

count_textures = 1

for png in files:
    count_textures += 1
    print("Compiling texture " + png)
    name = png.split(".")[0] + "Texture"
    procname = "draw" + name[0].upper() + name[1:]
    im = Image.open('textures/' + png)
    (w, h) = im.size

    alpha = im.split()[-1].load()
    hasalpha = False
    for y in range(h):
        for x in range(w):
            if not alpha[x, y] == 255:
                hasalpha = True
                break
        if hasalpha:
            break

    if count_textures % 10 == 0:
        include.write("\r\nGLOBAL " + procname + ":PROC")
    else:
        include.write(",\\\r\n       " + procname + ":PROC")

    output.write("PROC " + procname + "\n")
    output.write("ARG @@x:dword,\\\n")
    output.write("    @@y:dword\n")
    output.write("call drawTexture, offset " + name + ", [@@x], [@@y], " + str(1 if hasalpha else 0) + "\n")
    output.write("ret\n")
    output.write("ENDP\n\n")

    pix = im.load()

    if w <= 32 and h <= 32:
        dataseg += name + " dd " + str(w) + ", " + str(h) + "\n"

        for y in range(h):
            dataseg += "db"
            for x in range(w):
                dataseg += " " + str(misc.VGA.get_closest_index(pix[x, y]))
                if x < w - 1:
                    dataseg += ","
            dataseg += "\n"
    else:
        include.write(", load" + name + ":PROC")
        udataseg += name + " dd 2 dup (?)\n"
        udataseg += (" " * len(name)) + " db " + str(w*h) + " dup (?) \n\n"
        bmpname = png.split(".")[0] + ".bmp"
        bmp = open("textures/" + bmpname, "wb")
        bmp.write(struct.pack("I", w))
        bmp.write(struct.pack("I", h))
        for y in range(h):
            for x in range(w):
                bmp.write(struct.pack("B", 
                    misc.VGA.get_closest_index(pix[x, y])))
        bmp.close()

        dataseg += name + 'Meta db "textures\\' + bmpname + '", 0 \n'

        output.write("PROC load" + name + "\n")
        output.write("call loadFile, offset " + name + "Meta, offset " + name + "\n")
        output.write("ret\n")
        output.write("ENDP\n\n")
        texturesOnDisk.append(name)

output.write("PROC loadAllTextures\n")
for name in texturesOnDisk:
    output.write("call load" + name + "\n")
output.write("ret\n")
output.write("ENDP\n\n")

output.write(dataseg)
output.write(udataseg)
output.write("END\n")
output.close()
